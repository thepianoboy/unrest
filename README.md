# UnREST - A RESTful Game Server for UnCiv

## What is this project?

This project aims to serve as an alternative to the Play-by-Cloud functionality in [UnCiv](https://github.com/yairm210/Unciv).

## Why do we need this instead of UnCiv's built-in multiplayer?

Playing UnCiv with friends is fun, but the Dropbox-based multiplayer implementation leaves a lot to be desired. Ratelimiting and request limits cause gameplay interruptions and unexpected behavior for players. Similarly, developers are forced to work around these limitations and the related performance issues when designing new features or fixing old ones.

This is not to say that the UnCiv developers didn't do a great job with the tools available to them, but such a great game deserves equally great multiplayer support. This project aims to provide that.

## Maintainability

If you have concerns about the maintainability of this project, see the [documentation](docs/QUESTIONS.md#maintainability).

## Prerequisites

Building UnREST requires CMake and a C++20 compatible compiler.

## Building

Clone the repository and its submodules using your favorite Git client.

From the root of the cloned repository, use one of the following sets of commands: 

> These instructions are geared toward a Linux/Unix shell, but other configurations using a different OS or generator are known to work with only slight modifications.

### Ninja
```sh
mkdir build
cd build
cmake -G Ninja ..
ninja
```

### Unix Makefiles
```sh
mkdir build
cd build
cmake ..
make
```

## Running

From the `build` directory created in the previous step, invoke the program `./bin/unrest`. Command line options which can change the behavior of the server may be added in the future.

## RESTful Design

UnREST, as its name implies, aims to produce a RESTful API for synchronizing game data between UnCiv clients. The reason for this is threefold:

1. REST-based designs are ubiquitous and the tooling for working with them is equally widespread. Client developers should not have any difficulty in working with this server.
2. Any user with the requisite skills and access to hardware should be able to host a multiplayer server for public or private use. A RESTful design allows users to access a new server with only its human-readable URI.
3. Any developer who implements the same API could create a drop-in replacement for this project without noticably affecting the end user.

## Legacy API

The Legacy API for UnREST is designed to provide verbs that are compatible with the ones used by clients which connect to the Dropbox cloud.

```
/v1
│
└─ /v1/game
   │
   ├─ [POST]   - Create a new game with the given UUID (JSON): 
   │             { "gameId": "$UUID" }
   │             Returns: [201] On success
   │                      [507] The server is unable to manage any more game data
   │
   ├─ [GET]    - Download data for game specified by parameter: 
   │             ?gameId=$UUID
   │             Returns: [200] On success (with game data as content)
   │                      [404] The requested game doesn't exist
   │
   ├─ [PUT]    - Upload save data (JSON):
   │             {..., "gameID": "$UUID", ...}
   │             Returns: [200] On success
   │                      [410] The requested game doesn't exist (and don't keep trying)
   │                      [507] The server is unable to manage any more game data
   │
   ├─ [DELETE] - Delete game with the given UUID (JSON):
   │             { "gameId": "$UUID" }
   │             Returns: [200] On success
   │                      [410] The requested game doesn't exist (and don't keep trying)
   │
   ├─ /v1/game/lock (NOT YET IMPLEMENTED)
   │  │
   │  ├─ [POST]   - Lock save data for game specified by parameter:
   │  │             { "gameId": "$UUID" }
   │  │             Returns: [201] On success
   │  │                      [409] The game is already locked
   │  │
   │  └─ [DELETE] - Release lock on save data for game specified by parameter:
   │                 { "gameId": "$UUID" }
   │                 Returns: [200] On success
   │                          [403] The game was not locked
   │
   └─ /v1/game/preview (NOT YET IMPLEMENTED)
      │
      ├─ [POST]   - Do nothing. (game previews are generated on-request by the server) 
      │             Returns: [200] Always
      │
      └─ [GET]    - Download preview for game specified by parameter:
                    ?gameId=$UUID
                    Returns: [200] On success (with the game preview as content)
                             [404] The requested game doesn't exist
```

> Additionally, the server might return the following status codes for _any_ method:
> - `400` - The request was badly formatted or was missing a required parameter.
> - `500` - The server behaved in a way that was unexpected or otherwise not anticipated by the developers.

## Server Status API

```
/status
│
├─ [GET]       - Get information about the current server state
│               Returns: [200] On success (with status as content)
│
└─ /status/features (NOT YET IMPLEMENTED)
   │
   └─ [GET]    - Get a list of features supported by the server
                 Returns: [200] On success (with feature list as content)
```

## Second-Generation API

New features which extend the capabilities of UnREST beyond its parity with Dropbox-based cloud saves will be exposed as `/v2/` URIs.

## Future Development

Further development will be directed toward the implementation of additional server-dependent multiplayer features. 

Planned Features:
- Push notifications (via UnifiedPush)
- Turn timer / Vote to skip turn
- Vote to kick player
- Server authentication
- Compressed load / save
- Binary (BSON/CBOR/UBJSON) uploads

Features under consideration (PRs welcome!):
- Private (password-protected) games
- Game lobbies
- Differential load / save
- Turn notifications via email / social / webhook
- Game metrics
- In-Game Messaging
- Simultaneous Turns

> NOTICE: The availability of a particular feature in UnREST does not guarantee that it will be used in UnCiv. Adoption of most (if not all) of these features would require considerable cooperation between developers in both projects and that may not be possible.

> MAINTANER NOTE: It is the belief of the UnREST maintainer that, if the UnREST API is adopted, a workflow for would need to be negotiated which allows the referral of features which require server support to UnREST.


## Storage Backends

The UnREST server is designed to be able to take advantage of multiple storage backends. The abstract class `unrest::Store` provides an interface through which the server listener can perform CRUD operations with received data.

While it is hoped that any sufficiently sophisticated backend would provide ACID transactions, the server listener assumes that transactions provide _Atomicity_, _Consistency_, and _Isolation_. Adapters for backends missing one or more of the ACID properties should perform an appropriate accomodation in software:

For development purposes, the (incomplete) `unrest::StoreHashMap` implementation acts as a form of transient storage. A persistent storage method should be enabled for any expectation of reliability or data preservation.

## Disclaimer

UnREST is an independent project developed from an original codebase (except for its dependencies which were fetched from upstream sources by git submodule under the terms of their own open source licenses). Though it is intended as a server for UnCiv, UnREST is a product of its own. The contributions, claims, and actions of developers as they are related to UnREST DO NOT constitute contributions, claims, or actions on behalf of UnCiv or any other project.

## Credits

UnCiv is the work of Yair Morgenstern and a number of very talented community contributors. It is an open source project released under the terms of the Mozilla Public License 2.0. See [http://github.com/yairm210/Unciv](http://github.com/yairm210/Unciv) for more information.

