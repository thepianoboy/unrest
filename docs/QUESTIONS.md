# Answers to Questions About UnREST

## Contents

[API Standardization](#api-standardization)
- Is the API Stable?
- What are FROZEN and STABLE endpoints?
- Can endpoints be removed?


[Maintainability](#maintainability)
- What happens if you stop maintaining UnREST?
- What if there is a conflict between the UnREST API and UnCiv?
- What if a better implementation comes along?
- What will you do if somebody forks UnCiv?

## API Standardization

### Is the API Stable?

Yes and no. New URIs and methods should be considered volatile until they are marked as either **FROZEN** or **STABLE**.


### What are **FROZEN** and **STABLE** endpoints?

**FROZEN** endpoints are API components which have been documented in a major release. Changes which modify the behavior of these endpoints will not be integrated until the next major release.

**STABLE** endpoints have been used to implement features in a release of the official UnCiv client. Changes which modify the behavior of these endpoints will be rejected.


### Can endpoints be removed?

Yes. Volatile or undocumented endpoints can be removed at any time. 

**FROZEN** endpoints may be marked as **DEPRECATED** at any time.
**STABLE** endpoints which have not been used for at least two minor (i.e. 0.x.0) releases of UnCiv may be marked as **DEPRECATED**.

**DEPRECATED** endpoints may be removed or modified following the first major release of UnREST after their deprecation.



## Maintainability

### What happens if you stop maintaining UnREST?

From the maintainer:
> When I first decided to make a third-party implementation of a game server for UnCiv, I made some decisions about my exit strategy in advance so that the community could trust that even if I'm not around, the server multiplayer system won't fall apart.
> 
> First off, UnREST is completely free and open source. I chose the MPL v2.0 as a license because I wanted to make sure that the core of the API stayed open source even if it was forked by somebody else. That way, if I were to up and disappear, whoever took up the project after me would need to keep working with the community.
> 
> This was also part of the reason I chose to implement a RESTful multiplayer API instead of some ultra specialized and efficient communication protocol. Anyone can provide their own implementation and players need only point their client to a new server URI to use it.
>
> Personally, I don't anticipate a situation where I leave the project unmaintained while other folks are still trying to contribute to it. If, for some reason, I decided to leave the project, I plan to transfer ownership or at the very least provide a forwarding address to the project that replaces this one.


### What if there is a conflict between the UnREST API and UnCiv?

UnREST will defer to UnCiv in these cases. See [API Standardization](#api-standardization) for answers to similar questions.


### What if a better implementation comes along?

Great! So long as the UnREST API is maintained, users shouldn't see a difference. It's our hope that server hosts will just move on to whatever implementation works best for the users, and that's the most important thing.


### What will you do if somebody forks UnCiv?

Assuming that the forked client maintains compatibility with the REST API, it really won't matter. It's even theoretically possible that players could be on different clients within the same game. It should be noted that unless the community officially moves on to a different client (by vote or by Yair's recommendation) the API standardization track will still take its cues from the maintainers of the parent repository at [https://github.com/yairm210/Unciv](https://github.com/yairm210/Unciv).


