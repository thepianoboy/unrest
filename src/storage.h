/*
	Copyright (c) 2020, Colin Vanden Heuvel

	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at https://mozilla.org/MPL/2.0/.

	This Source Code Form is "Incompatible With Secondary Licenses" as
	defined by the Mozilla Public License, v. 2.0.
*/

#ifndef UNREST_STORAGE 
#define UNREST_STORAGE

#include <chrono>
#include <exception>
#include <mutex>
#include <string>
#include <unordered_map>

#include <nlohmann/json.hpp>

namespace unrest {


class Store {
public:

	struct Payload {
		std::chrono::time_point<std::chrono::steady_clock> last_modified;
		nlohmann::json data;
	};

	enum class Status {
		DUPLICATE,
		NOT_FOUND,
	};

	class Exception : public std::logic_error {
	public:
		Exception(const std::string& what_arg, Status s) 
			: std::logic_error(what_arg), _status(s) {};
		Exception(const char* what_arg, Status s)
			: std::logic_error(what_arg), _status(s) {};

		Status kind() {
			return _status;
		}
	private:
		Status _status;
	};
	
	virtual ~Store() = default;

	// Required Methods
	virtual void Create(const std::string& key, const Payload& data) = 0; 
	virtual void Update(const std::string& key, const Payload& data) = 0;
	virtual Payload Read(const std::string& key) = 0;
	virtual void Delete(const std::string& key) = 0;

	// Required for databases which do not automatically write to persistent storage
	virtual void Persist(); 

protected:
	// Required for databases which do not guarantee atomic transactions  
	virtual void Lock();
	virtual void Unlock();

};


class StoreHashMap : public Store {
public:
	void Create(const std::string& key, const Store::Payload& data);
	Store::Payload Read(const std::string& key);
	void Update(const std::string& key, const Store::Payload& data);
	void Delete(const std::string& key);

	void Persist();

private:
	void Lock();
	void Unlock();

	std::unordered_map<std::string, Store::Payload> _data;
	std::mutex _global_lock;
};

} // namespace unrest


#endif

