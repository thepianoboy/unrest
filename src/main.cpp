/*
	Copyright (c) 2020, Colin Vanden Heuvel

	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at https://mozilla.org/MPL/2.0/.

	This Source Code Form is "Incompatible With Secondary Licenses" as
	defined by the Mozilla Public License, v. 2.0.
*/

#include <iostream>
#include <string>
#include <vector>

#include "api.h"
#include "storage.h"

int main(int argc, char** argv) {

	// initialize a :std::map of GameInfo objects keyed by gameIDs
	// ...

	std::string pattern_UUID = "([[:xdigit:]]{8}-([[:xdigit:]]{4}-){3}-[[:xdigit:]]{12})";
	std::string pattern_gameID = "/game/" + pattern_UUID;

	unrest::Listener server("0.0.0.0", 8000, std::move(std::make_unique<unrest::StoreHashMap>()));
	server.bind_legacy_endpoints();
	server.listen();

	return 0;
}

