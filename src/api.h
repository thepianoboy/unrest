/*
	Copyright (c) 2020, Colin Vanden Heuvel

	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at https://mozilla.org/MPL/2.0/.

	This Source Code Form is "Incompatible With Secondary Licenses" as
	defined by the Mozilla Public License, v. 2.0.
*/

#ifndef __UNREST_API_H__
#define __UNREST_API_H__

#include <iostream>

#include <array>
#include <chrono>
#include <string>
#include <vector>
#include <memory>
#include <mutex>

#define CPPHTTPLIB_ZLIB_SUPPORT
#include <httplib.h>

#include <nlohmann/json.hpp>

#include "storage.h"

namespace unrest {

class Listener {
public:
	Listener(const std::string& ip, uint16_t port, std::unique_ptr<Store> storage_engine);

	// Set up default listening endpoints
	void bind_legacy_endpoints();

	// Wait for connections
	void listen();

	static std::string json_from_http_error(int status, const std::string what = "");

private:
	// Add a game endpoint
	void add_game(nlohmann::json gamedata);

	httplib::Server _srv;
	std::string _ip;
	uint16_t _port;
	std::chrono::time_point<std::chrono::steady_clock> _basetime;
	std::unique_ptr<unrest::Store> _storage;
};


}

#endif
