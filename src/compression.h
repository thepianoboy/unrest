/*
    Copyright (c) 2020, Colin Vanden Heuvel

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at https://mozilla.org/MPL/2.0/.

    This Source Code Form is "Incompatible With Secondary Licenses" as
    defined by the Mozilla Public License, v. 2.0.
*/

#ifndef UNREST_COMPRESSION_H
#define UNREST_COMPRESSION_H

#include <vector>
#include <string>

namespace unrest {

// TODO: Make a generalized compressor class 
class Gzip {	
public:
	static std::string compress(const std::string& in);
	static std::string decompress(const std::string& in);
};

} // namespace unrest 

#endif

