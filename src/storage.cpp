/*
	Copyright (c) 2020, Colin Vanden Heuvel

	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at https://mozilla.org/MPL/2.0/.

	This Source Code Form is "Incompatible With Secondary Licenses" as
	defined by the Mozilla Public License, v. 2.0.
*/

#include "storage.h"

//
// Stubs for OPTIONAL Store methods
//

namespace unrest {

void Store::Persist() {
	;
}

void Store::Lock() {
	;
}

void Store::Unlock() {
	;
}


//
// In-memory backend using hash map storage 
//

void StoreHashMap::Create(
	const std::string& key, 
	const Store::Payload& data
) {
	this->Lock();
	auto result = this->_data.try_emplace(key, data);
	this->Unlock();

	if (!result.second) {
		throw Store::Exception("Attempted to CREATE duplicate key!", Status::DUPLICATE);
	}
}

Store::Payload StoreHashMap::Read(const std::string& key) {
	std::lock_guard<std::mutex> lock(this->_global_lock);
	return this->_data.at(key);
}

void StoreHashMap::Update(
	const std::string& key,
	const Store::Payload& data
) {
	try {
		std::lock_guard<std::mutex> lock(this->_global_lock);
		this->_data.at(key) = data;
	}
	catch (std::out_of_range e) {
		throw Store::Exception(e.what(), Status::NOT_FOUND);
	}
}

void StoreHashMap::Delete(const std::string& key) {
	if (this->_data.contains(key)) {
		std::lock_guard<std::mutex> lock(this->_global_lock);
		this->_data.erase(key);
	}	
	else {
		throw Store::Exception("Attempted to DELETE nonexistent key!", Status::NOT_FOUND);
	}
}


void StoreHashMap::Lock() {
	this->_global_lock.lock();	
}

void StoreHashMap::Unlock() {
	this->_global_lock.unlock();
}


void StoreHashMap::Persist() {
	;	
}


} // namespace unrest
