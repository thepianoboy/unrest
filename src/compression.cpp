/*
    Copyright (c) 2020, Colin Vanden Heuvel

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at https://mozilla.org/MPL/2.0/.

    This Source Code Form is "Incompatible With Secondary Licenses" as
    defined by the Mozilla Public License, v. 2.0.
*/

#include "compression.h"

#include <gzip/compress.hpp>
#include <gzip/decompress.hpp>

namespace unrest {

std::string Gzip::compress(const std::string& in) {
	return gzip::compress(in.data(), in.size());
}

std::string Gzip::decompress(const std::string& in) {
	return gzip::decompress(in.data(), in.size());
}

} // namespace unrest


