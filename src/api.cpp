/*
	Copyright (c) 2020, Colin Vanden Heuvel

	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at https://mozilla.org/MPL/2.0/.

	This Source Code Form is "Incompatible With Secondary Licenses" as
	defined by the Mozilla Public License, v. 2.0.
*/

#include "api.h"
#include "compression.h"

#include <functional>

namespace unrest {

/*
    /
    |
    |-- /status
    |
    .-- /game - POST: add a new game / DELETE: clean up a game
        |
        |-- /game/load - download a save file for an existing game
        |
        |-- /game/save - upload a save file for an existing game
        |
        |-- /game/lock // TODO: Return 200 if nobody has the lock, else 409
        |
        .-- /game/api/(...) // TODO: RESTful state representation
*/

Listener::Listener(
	const std::string& ip, 
	uint16_t port, 
	std::unique_ptr<Store> storage_engine
) : _ip(ip), _port(port), _storage(std::move(storage_engine)) {
	this->_basetime = std::chrono::steady_clock::now();
}

void Listener::bind_legacy_endpoints() {
	this->_srv.Get("/", [](const httplib::Request&, httplib::Response& res) {
		res.set_content("Hello, world!", "text/plain");
	});

	this->_srv.Get("/status", [this](const httplib::Request&, httplib::Response& res) {
		nlohmann::json payload;
		std::chrono::duration<double, std::milli> uptime = 
			std::chrono::steady_clock::now() - this->_basetime;
		payload["uptime"] = uptime.count();
		res.status = 200;
		res.set_content(payload.dump(), "application/json");
	});

	this->_srv.Post("/game", [this](const httplib::Request& req, httplib::Response& res) {
		
		std::string body_ungz = Gzip::decompress(req.body);
		
		auto body = nlohmann::json::parse(body_ungz);

		if (!body.contains("gameId")) {
			res.status = 400;
			res.set_content(json_from_http_error(res.status), "application/json");
		}
		else {
			try {	
				this->add_game(body);
				res.status = 201;
				res.set_content("{}", "application/json");
			}
			catch (std::exception& e) {
				res.status = 507;
				res.set_content(json_from_http_error(res.status), "application/json");
			}
		}
	});

	this->_srv.Delete("/game", [this](const httplib::Request& req, httplib::Response& res) {
		auto body = nlohmann::json::parse(req.body);
		if (!body.contains("gameId")) {
			res.status = 400;
			res.set_content(json_from_http_error(res.status), "application/json");
			return;
		}

		try {
			this->_storage->Delete(body["gameId"]);
			res.status = 200;
			res.set_content("{}", "application/json");
		}
		catch (unrest::Store::Exception& e) {
			if (e.kind() == unrest::Store::Status::NOT_FOUND) {
				res.status = 410;
				res.set_content(json_from_http_error(res.status), "application/json");
			}
			else {
				res.status = 500;
				res.set_content(json_from_http_error(res.status), "application/json");
			}
		}

	});

	this->_srv.Put(
		"/game",
		[this](const httplib::Request& req, httplib::Response& res) {

			std::string body_ungz = Gzip::decompress(req.body);

			auto body = nlohmann::json::parse(body_ungz);

			if (!body.contains("gameId")) {
				res.status = 400;
				res.set_content(json_from_http_error(res.status), "application/json");
				return;
			}
			try {
				unrest::Store::Payload incoming_data;
				incoming_data.data = body;
				incoming_data.last_modified = std::chrono::steady_clock::now();
				this->_storage->Update(body["gameId"], incoming_data);
				res.status = 200;
				res.set_content("{}", "application/json");
			}
			catch (unrest::Store::Exception& e) {
				if (e.kind() == unrest::Store::Status::NOT_FOUND) {
					res.status = 410;
					res.set_content(json_from_http_error(res.status), "application/json");
				}
				else {
					res.status = 507;
					res.set_content(json_from_http_error(res.status), "application/json");
				}
			}
		}
	);

	this->_srv.Get(
		"/game",
		[this](const httplib::Request& req, httplib::Response& res) {
			if (!req.has_param("gameId")) {
				res.status = 400;
				res.set_content(json_from_http_error(res.status), "application/json");
				return;
			}

			try {
				auto payload = this->_storage->Read(req.get_param_value("gameId")).data;
				res.status = 200;
				res.set_content(Gzip::compress(payload.dump()), "application/json");
			}
			catch (std::out_of_range& e) {
				res.status = 404;
				res.set_content(json_from_http_error(res.status), "application/json");
			}
			catch (std::exception& e) {
				res.status = 500;
				res.set_content(json_from_http_error(res.status), "application/json");
			}
		}
	);

}

void Listener::add_game(nlohmann::json gamedata) {

	unrest::Store::Payload payload;
	payload.last_modified = std::chrono::steady_clock::now();
	payload.data = gamedata;
	std::string uuid = gamedata.at("gameId");

	this->_storage->Create(gamedata.at("gameId"), payload);


}

void Listener::listen() {
	_srv.listen(_ip.c_str(), _port);
}


std::string Listener::json_from_http_error(int status, const std::string what) {
	
	std::string _what(what);
	
	if (_what.empty()) {
		switch (status) {
			case 200:
				_what = "OK";
				break;
			case 201:
				_what = "Created";
				break;
			case 400:
				_what = "Bad Request: Malformed game specification";
				break;
			case 404:
				_what = "Bad Request: Game not found";
				break;
			case 409:
				_what = "Bad Request: Another player is modifying the game";
				break;
			case 410:
				_what = "Bad Request: Game not found";
				break;
			case 500:
				_what = "Server Error: Internal error";
				break;
			case 503:
				_what = "Server Error: Service unavailable";
				break;
			case 507:
				_what = "Server Error: The server is full and cannot accept any more games";
				break;
			default:
			_what = "Unknown Error: The server did something unexpected";
				break;
		}
	}

	return R"({"status": )" + std::to_string(status) + R"(, "what": ")" + _what + R"("})";
}


} // namespace unrest
